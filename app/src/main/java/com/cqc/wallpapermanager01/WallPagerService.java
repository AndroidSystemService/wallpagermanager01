package com.cqc.wallpapermanager01;

import android.app.AlarmManager;
import android.app.Service;
import android.app.WallpaperManager;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;

import java.io.IOException;

/**
 * Created by ${cqc} on 2017/5/9.
 */

public class WallPagerService extends Service {

    private WallpaperManager manager;
    int[] pagers = {R.mipmap.a, R.mipmap.b, R.mipmap.c,};
    int current = 0;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        manager = WallpaperManager.getInstance(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (current > 2) {
            current = 0;
        }
        try {
            manager.setResource(pagers[current++]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Service.START_STICKY;
    }
}
